﻿using System;
using System.Collections.Generic;
using Refit;
using Newtonsoft.Json;
using Xamarin;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Forms;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using Akavache;

namespace CodeChallenge
{
	public class QualificationListPageVM : BaseNotify
	{
		public ObservableCollection<Qualification> QList{ get; protected set; }

		public ICommand LoadQualifications
		{
			get
			{
				return new Command(async() =>
					await LoadQualificationsTask());
			}
		}

		bool _isBusy = false;

		public bool IsBusy
		{
			get
			{
				return _isBusy;
			}
			set
			{
				SetPropertyChanged(ref _isBusy, value);
				SetPropertyChanged("IsNotBusy");
			}
		}

		public bool IsNotBusy
		{
			get
			{
				return !IsBusy;
			}
		}

		public ICommand RefreshQualifications
		{
			get
			{
				return new Command(async() =>
					await RefreshQualificationsTask());
			}
		}

		public async Task RefreshQualificationsTask()
		{
			IsBusy = true;
			await LoadQualificationsTask();
			IsBusy = false;
		}

		public async Task LoadQualificationsTask()
		{
			BlobCache.UserAccount.GetAndFetchLatest("Qualifications",		
				async () =>
				{
					//Make the HTTP call to fetch the data
					var gojimoApi = RestService.For<IGojimoApi>(Keys.GojimoApiBaseUrl);
					return await gojimoApi.GetQualifications();					
				}).Subscribe(cachedThenUpdated =>
				{
					if (!QList.SequenceEqual(cachedThenUpdated))
					{
						QList.Clear();
						cachedThenUpdated.ForEach(QList.Add);
					}					
					QList.ToList().ForEach(x => Console.WriteLine(x.name));
				}, async (Exception obj) =>
				{
					Insights.Report(obj);
					Console.WriteLine(obj);
					//Inform the user. Can be handled better with different types of exceptions.
					Device.BeginInvokeOnMainThread(async () => await App.Current.MainPage.Navigation.NavigationStack.Last().DisplayAlert("Oops", "Cannot retrieve latest data. Ensure your connection", "Ok"));
				}, async () =>
				{
				});
		}

		public QualificationListPageVM()
		{
			QList = new ObservableCollection<Qualification>();
			LoadQualifications.Execute(null);
		}
	}
}

