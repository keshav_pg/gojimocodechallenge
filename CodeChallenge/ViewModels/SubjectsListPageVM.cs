﻿using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace CodeChallenge
{
	public class SubjectsListPageVM
	{
		public ObservableCollection<SubjectItemVM> SList{ get; protected set; }

		public SubjectsListPageVM(Qualification q)
		{
			SList = new ObservableCollection<SubjectItemVM>();
			q.subjects.ForEach(s =>
				SList.Add(new SubjectItemVM(s)));
		}
	}

	public class SubjectItemVM
	{
		public string title { get; protected set; }

		public Color colour { get; protected set; }

		public SubjectItemVM(Subject s)
		{
			title = s.title;
			if (!String.IsNullOrEmpty(s.colour))
				colour = Color.FromHex(s.colour);
			else
				colour = Color.Transparent;
		}
	}
}

