﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Refit;

namespace CodeChallenge
{
	public interface IGojimoApi
	{
		[Get("/api/v4/qualifications")]
		Task<List<Qualification>> GetQualifications();
	}
}

