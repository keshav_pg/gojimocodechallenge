﻿using System;

using Xamarin.Forms;

namespace CodeChallenge
{
	public class App : Application
	{
		public App()
		{			
			Akavache.BlobCache.ApplicationName = "CodeChallenge";
			// The root page of your application
			MainPage = new NavigationPage(new QualificationsListPage());           
		}

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}

