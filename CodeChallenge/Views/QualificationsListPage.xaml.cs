﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace CodeChallenge
{
	public partial class QualificationsListPage : ContentPage
	{
		public QualificationsListPage()
		{
			this.BindingContext = new QualificationListPageVM();
			InitializeComponent();
			Console.WriteLine("Hello");
		}

		public void OnQualificationItemTapped(object s, ItemTappedEventArgs e)
		{
			var q = e.Item as Qualification;
			Console.WriteLine(q.name);

			//Check if Qualification has Subjects
			if (q.subjects.Count != 0)
				Navigation.PushAsync(new SubjectsListPage(q));
			else
				DisplayAlert("Oops", "No subjects in this qualification", "Ok");

			//Deselect the item
			(s as ListView).SelectedItem = null;
		}
	}
}

