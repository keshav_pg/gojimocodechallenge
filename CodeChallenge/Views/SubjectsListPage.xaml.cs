﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace CodeChallenge
{
	public partial class SubjectsListPage : ContentPage
	{
		public SubjectsListPage(Qualification q)
		{
			this.BindingContext = new SubjectsListPageVM(q);
			InitializeComponent();
		}
	}
}

