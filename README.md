# README #

### What is this repository for? ###

[Gojimo Code Challenge](https://drive.google.com/file/d/0B7aAQ1zCpEcnU0FZXzdhZjkxeF9GN2pRdHpnN1pjNmowbkc4/view?usp=sharing)

### Technology Stack? ###

Xamarin.Forms - Tested on iOS and Android. Should be ready to create the Windows Universal App via Visual Studio too.

--Nugets--
[Refit](https://github.com/paulcbetts/refit) 

[Akavache](https://github.com/akavache/Akavache)

[Xamarin Insights](https://xamarin.com/insights)

Pull To Refresh Feature is also implemented to refresh the qualifications on user request.